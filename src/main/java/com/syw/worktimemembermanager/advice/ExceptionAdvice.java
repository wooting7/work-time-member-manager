package com.syw.worktimemembermanager.advice;

import com.syw.worktimemembermanager.enums.ResultCode;
import com.syw.worktimemembermanager.exception.*;
import com.syw.worktimemembermanager.model.common.CommonResult;
import com.syw.worktimemembermanager.service.common.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }




    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }




    @ExceptionHandler(CNoMemberDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoMemberDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_MEMBER_DATA);
    }
    @ExceptionHandler(CNoUsernameDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoUsernameDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_USERNAME_DATA);
    }

    @ExceptionHandler(CWrongPasswordDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPasswordDataException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PASSWORD_DATA);
    }

    @ExceptionHandler(COutMemberDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, COutMemberDataException e) {
        return ResponseService.getFailResult(ResultCode.OUT_MEMBER_DATA);
    }

    @ExceptionHandler(CNotMachNewPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotMachNewPasswordException e) {
        return ResponseService.getFailResult(ResultCode.NOT_MACH_NEW_PASSWORD);
    }




    @ExceptionHandler(CAlreadyInWorkTimeRecordDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CAlreadyInWorkTimeRecordDataException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_IN_WORK_TIME_RECORD_DATA);
    }

    @ExceptionHandler(CNoInWorkTImeRecord.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoInWorkTImeRecord e) {
        return ResponseService.getFailResult(ResultCode.NO_IN_WORK_TIME_RECORD);
    }

    @ExceptionHandler(CNotChangedOutWorkTimeRecord.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotChangedOutWorkTimeRecord e) {
        return ResponseService.getFailResult(ResultCode.NOT_CHANGED_OUT_WORK_TIME_RECORD);
    }

    @ExceptionHandler(CNotChangedAlreadyInWorkTimeRecord.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotChangedAlreadyInWorkTimeRecord e) {
        return ResponseService.getFailResult(ResultCode.NOT_CHANGED_ALREADY_IN_WORK_TIME_RECORD);
    }

    @ExceptionHandler(CNotChangedSameWorkTimeRecord.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotChangedSameWorkTimeRecord e) {
        return ResponseService.getFailResult(ResultCode.NOT_CHANGED_SAME_WORK_TIME_RECORD);
    }




    @ExceptionHandler(CExitApproveState.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CExitApproveState e) {
        return ResponseService.getFailResult(ResultCode.EXIT_APPROVE_STATE);
    }

    @ExceptionHandler(CNoMinusValueException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoMinusValueException e) {
        return ResponseService.getFailResult(ResultCode.NO_MINUS_VALUE);
    }

    @ExceptionHandler(CNoSameDateRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoSameDateRequestException e) {
        return ResponseService.getFailResult(ResultCode.NO_SAME_DATE_REQUEST);
    }

    @ExceptionHandler(CBeShortOfVacationTotalException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CBeShortOfVacationTotalException e) {
        return ResponseService.getFailResult(ResultCode.BE_SHORT_OF_VACATION_TOTAL);
    }
}
