package com.syw.worktimemembermanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkTimeMemberManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkTimeMemberManagerApplication.class, args);
	}

}
