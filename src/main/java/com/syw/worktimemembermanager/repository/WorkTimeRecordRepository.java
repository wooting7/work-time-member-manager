package com.syw.worktimemembermanager.repository;

import com.syw.worktimemembermanager.entity.VacationRecord;
import com.syw.worktimemembermanager.entity.WorkTimeRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

public interface WorkTimeRecordRepository extends JpaRepository<WorkTimeRecord, Long> {
    Optional<WorkTimeRecord> findByDateWorkAndMember_Id(LocalDate dataWork, long memberId);
    List<WorkTimeRecord> findAllByDateWork(LocalDate dateWork);

    List<WorkTimeRecord> findAllByMember_IdAndDateWorkGreaterThanEqualAndDateWorkLessThanEqual(Long memberId, LocalDate dateStart, LocalDate dateEnd);


}
