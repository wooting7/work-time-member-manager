package com.syw.worktimemembermanager.repository;

import com.syw.worktimemembermanager.entity.VacationCount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VacationCountRepository extends JpaRepository<VacationCount, Long> {
    Optional<VacationCount> findByMemberId(long memberId);
}
