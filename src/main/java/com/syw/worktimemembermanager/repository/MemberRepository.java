package com.syw.worktimemembermanager.repository;

import com.syw.worktimemembermanager.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsernameAndPowerGrade(String username, boolean powerGrade);
}
