package com.syw.worktimemembermanager.repository;

import com.syw.worktimemembermanager.entity.VacationRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface VacationRecordRepository extends JpaRepository<VacationRecord, Long> {
    int countByDateRequestAndMember_Id(LocalDate dateRequest, long memberId);

    List<VacationRecord> findByMember_Id(long memberId);

    List<VacationRecord> findAllByMember_IdAndDateRequestGreaterThanEqualAndDateRequestLessThanEqual(Long memberId, LocalDate dateStart, LocalDate dateEnd);



}
