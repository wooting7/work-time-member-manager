package com.syw.worktimemembermanager.exception;

public class CNotChangedOutWorkTimeRecord extends RuntimeException {
    public CNotChangedOutWorkTimeRecord(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotChangedOutWorkTimeRecord(String msg) {
        super(msg);
    }

    public CNotChangedOutWorkTimeRecord() {
        super();
    }
}
