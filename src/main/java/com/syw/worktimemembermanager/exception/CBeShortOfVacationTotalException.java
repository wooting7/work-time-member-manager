package com.syw.worktimemembermanager.exception;

public class CBeShortOfVacationTotalException extends RuntimeException {
    public CBeShortOfVacationTotalException(String msg, Throwable t) {
        super(msg, t);
    }

    public CBeShortOfVacationTotalException(String msg) {
        super(msg);
    }

    public CBeShortOfVacationTotalException() {
        super();
    }
}