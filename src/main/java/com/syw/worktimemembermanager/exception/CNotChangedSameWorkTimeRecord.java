package com.syw.worktimemembermanager.exception;

public class CNotChangedSameWorkTimeRecord extends RuntimeException {
    public CNotChangedSameWorkTimeRecord(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotChangedSameWorkTimeRecord(String msg) {
        super(msg);
    }

    public CNotChangedSameWorkTimeRecord() {
        super();
    }
}