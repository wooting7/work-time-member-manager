package com.syw.worktimemembermanager.exception;

public class CNotMachNewPasswordException extends RuntimeException{
    public CNotMachNewPasswordException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotMachNewPasswordException(String msg) {
        super(msg);
    }

    public CNotMachNewPasswordException() {
        super();
    }
}