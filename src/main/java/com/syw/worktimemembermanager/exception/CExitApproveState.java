package com.syw.worktimemembermanager.exception;

public class CExitApproveState extends RuntimeException {
    public CExitApproveState(String msg, Throwable t) {
        super(msg, t);
    }

    public CExitApproveState(String msg) {
        super(msg);
    }

    public CExitApproveState() {
        super();
    }
}
