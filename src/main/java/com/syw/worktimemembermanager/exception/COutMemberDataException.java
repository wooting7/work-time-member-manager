package com.syw.worktimemembermanager.exception;

public class COutMemberDataException extends RuntimeException{
    public COutMemberDataException(String msg, Throwable t) {
        super(msg, t);
    }

    public COutMemberDataException(String msg) {
        super(msg);
    }

    public COutMemberDataException() {
        super();
    }
}
