package com.syw.worktimemembermanager.exception;

public class CNoMinusValueException extends RuntimeException {
    public CNoMinusValueException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoMinusValueException(String msg) {
        super(msg);
    }

    public CNoMinusValueException() {
        super();
    }
}
