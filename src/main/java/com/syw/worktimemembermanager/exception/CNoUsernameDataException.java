package com.syw.worktimemembermanager.exception;

public class CNoUsernameDataException extends RuntimeException{
    public CNoUsernameDataException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoUsernameDataException(String msg) {
        super(msg);
    }

    public CNoUsernameDataException() {
        super();
    }
}
