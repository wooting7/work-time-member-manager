package com.syw.worktimemembermanager.exception;

public class CAlreadyInWorkTimeRecordDataException extends RuntimeException{

    public CAlreadyInWorkTimeRecordDataException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAlreadyInWorkTimeRecordDataException(String msg) {
        super(msg);
    }

    public CAlreadyInWorkTimeRecordDataException() {
        super();
    }
}
