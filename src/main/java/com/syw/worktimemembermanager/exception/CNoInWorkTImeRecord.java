package com.syw.worktimemembermanager.exception;

public class CNoInWorkTImeRecord extends RuntimeException {
    public CNoInWorkTImeRecord(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoInWorkTImeRecord(String msg) {
        super(msg);
    }

    public CNoInWorkTImeRecord() {
        super();
    }
}
