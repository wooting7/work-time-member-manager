package com.syw.worktimemembermanager.exception;

public class CNotChangedAlreadyInWorkTimeRecord extends RuntimeException {
    public CNotChangedAlreadyInWorkTimeRecord(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotChangedAlreadyInWorkTimeRecord(String msg) {
        super(msg);
    }

    public CNotChangedAlreadyInWorkTimeRecord() {
        super();
    }
}
