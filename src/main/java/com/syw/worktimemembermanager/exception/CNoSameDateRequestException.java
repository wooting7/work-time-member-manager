package com.syw.worktimemembermanager.exception;

public class CNoSameDateRequestException extends RuntimeException {
    public CNoSameDateRequestException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoSameDateRequestException(String msg) {
        super(msg);
    }

    public CNoSameDateRequestException() {
        super();
    }
}