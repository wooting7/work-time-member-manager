package com.syw.worktimemembermanager.model.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DateSearchRequest {
    @ApiModelProperty(notes = "사원 시퀀스", required = true)
    @NotNull
    private Long memberId;

    @ApiModelProperty(notes = "기준 년도", required = true)
    @NotNull
    private Integer dateStandardYear;

    @ApiModelProperty(notes = "기준 달", required = true)
    @NotNull
    private Integer dateStandardMonth;
}
