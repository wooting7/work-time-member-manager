package com.syw.worktimemembermanager.model.work;

import com.syw.worktimemembermanager.entity.WorkTimeRecord;
import com.syw.worktimemembermanager.intefaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkTimeRecodeStatusResponse {

    // 근무상태 보여주기 위한 정보
    @ApiModelProperty(value = "근무상태")
    private String workState;

    @ApiModelProperty(value = "근무상태명")
    private String workStateName;

    @ApiModelProperty(notes = "출근시간")
    private LocalTime dateStart;

    @ApiModelProperty(notes = "조퇴시간")
    private LocalTime dateEarlyLeave;

    @ApiModelProperty(notes = "퇴근시간")
    private LocalTime dateEnd;

    private WorkTimeRecodeStatusResponse(WorkTimeRecodeStatusResponseBuilder builder) {
        this.workState = builder.workState;
        this.workStateName = builder.workStateName;
        this.dateStart = builder.dateStart;
        this.dateEarlyLeave = builder.dateEarlyLeave;
        this.dateEnd = builder.dateEnd;
    }

    public static class WorkTimeRecodeStatusResponseBuilder implements CommonModelBuilder<WorkTimeRecodeStatusResponse> {
        private final String workState;

        private final String  workStateName;

        private final LocalTime dateStart;
        private final LocalTime dateEarlyLeave;
        private final LocalTime dateEnd;

        public WorkTimeRecodeStatusResponseBuilder(WorkTimeRecord workTimeRecord ) {
            this.workState = workTimeRecord.getWorkState().toString();
            this.workStateName = workTimeRecord.getWorkState().getName();
            this.dateStart = workTimeRecord.getDateStart();
            this.dateEarlyLeave = workTimeRecord.getDateEarlyLeave();
            this.dateEnd = workTimeRecord.getDateEnd();
        }

        @Override
        public WorkTimeRecodeStatusResponse build() {
            return new  WorkTimeRecodeStatusResponse(this);
        }
    }
}
