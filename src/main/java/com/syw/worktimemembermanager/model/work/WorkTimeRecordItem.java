package com.syw.worktimemembermanager.model.work;


import com.syw.worktimemembermanager.entity.WorkTimeRecord;
import com.syw.worktimemembermanager.enums.WorkState;
import com.syw.worktimemembermanager.intefaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class WorkTimeRecordItem {
    // 현재상태 보여주기 위한 모든 정보
    @ApiModelProperty(notes = "사원시퀀스")
    private Long memberId;
    @ApiModelProperty(notes = "근무일")
    private LocalDate dateWork;
    @ApiModelProperty(notes = "근무상태")
    private WorkState workState;
    @ApiModelProperty(notes = "출근시간")
    private LocalTime dateStart;
    @ApiModelProperty(notes = "조퇴시간")
    private LocalTime dateEarlyLeave;
    @ApiModelProperty(notes = "퇴근시간")
    private LocalTime dateEnd;

    public WorkTimeRecordItem(WorkTimeRecordItemBuilder builder) {
        this.memberId = builder.memberId;
        this.dateWork = builder.dateWork;
        this.workState = builder.workState;
        this.dateStart = builder.dateStart;
        this.dateEarlyLeave = builder.dateEarlyLeave;
        this.dateEnd = builder.dateEnd;
    }

    public static class WorkTimeRecordItemBuilder implements CommonModelBuilder<WorkTimeRecordItem> {
        private final Long memberId;
        private final LocalDate dateWork;
        private final WorkState workState;
        private final LocalTime dateStart;
        private final LocalTime dateEarlyLeave;
        private final LocalTime dateEnd;

        public WorkTimeRecordItemBuilder(WorkTimeRecord workTimeRecord) {
            this.memberId = workTimeRecord.getMember().getId();
            this.dateWork = workTimeRecord.getDateWork();
            this.workState = workTimeRecord.getWorkState();
            this.dateStart = workTimeRecord.getDateStart();
            this.dateEarlyLeave = workTimeRecord.getDateEarlyLeave();
            this.dateEnd = workTimeRecord.getDateEnd();
        }

        @Override
        public WorkTimeRecordItem build() {
            return new WorkTimeRecordItem(this);
        }
    }
}
