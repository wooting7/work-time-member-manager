package com.syw.worktimemembermanager.model.work;

import com.syw.worktimemembermanager.entity.WorkTimeRecord;
import com.syw.worktimemembermanager.intefaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkTimeAdminResponse {
    @ApiModelProperty(notes = "근무일")
    private LocalDate dateWork;

    @ApiModelProperty(notes = "출근시간")
    private LocalTime dateStart;

    @ApiModelProperty(notes = "조퇴시간")
    private LocalTime dateEarlyLeave;

    @ApiModelProperty(notes = "퇴근시간")
    private LocalTime dateEnd;

    @ApiModelProperty(notes = "사원명")
    private String memberName;


    @ApiModelProperty(notes = "총인원")
    private int totalMemberCount;

    @ApiModelProperty(notes = "출근")
    private int companyInCount;

    @ApiModelProperty(notes = "출근미등록")
    private int noCompanyInCount;

    @ApiModelProperty(notes = "조퇴")
    private int companyOutCount;

    @ApiModelProperty(notes = "퇴근")
    private int companyEarlyOutCount;

    public WorkTimeAdminResponse(MemberAdminResponseBuilder builder) {
        this.dateWork = builder.dateWork;
        this.dateStart = builder.dateStart;
        this.dateEarlyLeave = builder.dateEarlyLeave;
        this.dateEnd = builder.dateEnd;
        this.memberName = builder.memberName;
    }

    public static class MemberAdminResponseBuilder implements CommonModelBuilder<WorkTimeAdminResponse> {
        private final LocalDate dateWork;
        private final LocalTime dateStart;
        private final LocalTime dateEarlyLeave;
        private final LocalTime dateEnd;
        private final String memberName;

        public MemberAdminResponseBuilder(WorkTimeRecord workTimeRecord) {
            this.dateWork = workTimeRecord.getDateWork();
            this.dateStart = workTimeRecord.getDateStart();
            this.dateEarlyLeave = workTimeRecord.getDateEarlyLeave();
            this.dateEnd = workTimeRecord.getDateEnd();
            this.memberName = workTimeRecord.getMember().getMemberName();
        }

        @Override
        public WorkTimeAdminResponse build() {
            return new WorkTimeAdminResponse(this);
        }
    }
}
