package com.syw.worktimemembermanager.model.vacationRecord;

import com.syw.worktimemembermanager.entity.VacationRecord;
import com.syw.worktimemembermanager.enums.ApproveState;
import com.syw.worktimemembermanager.enums.VacationName;
import com.syw.worktimemembermanager.intefaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class VacationRecordItem {
    @ApiModelProperty(value = "시퀀스")
    private Long id;

    @ApiModelProperty(value = "사원시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "시작시간")
    private String timeStart;

    @ApiModelProperty(notes = "종료시간")
    private String timeEnd;

    @ApiModelProperty(notes = "요청일자")
    private LocalDate dateRequest;

    @ApiModelProperty(notes = "근태명")
    private VacationName vacationName;

    @ApiModelProperty(notes = "상세내역")
    private String detail;

    @ApiModelProperty(notes = "결제상태")
    private ApproveState approveState;

    @ApiModelProperty(notes = "생성일")
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "수정일")
    private LocalDateTime  dateUpdate;

    private VacationRecordItem(VacationItemBuilder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.timeStart = builder.timeStart;
        this.timeEnd = builder.timeEnd;
        this.dateRequest = builder.dateRequest;
        this.vacationName = builder.vacationName;
        this.detail = builder.detail;
        this.approveState = builder.approveState;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class VacationItemBuilder implements CommonModelBuilder<VacationRecordItem> {
        private final Long id;
        private final Long memberId;
        private final String timeStart;
        private final String timeEnd;
        private final LocalDate dateRequest;
        private final VacationName vacationName;
        private final String detail;
        private final ApproveState approveState;
        private final LocalDateTime dateCreate;
        private final LocalDateTime  dateUpdate;

    public VacationItemBuilder(VacationRecord vacationRecord) {
        this.id = vacationRecord.getId();
        this.memberId = vacationRecord.getMember().getId();
        this.timeStart = vacationRecord.getTimeStart();
        this.timeEnd = vacationRecord.getTimeEnd();
        this.dateRequest = vacationRecord.getDateRequest();
        this.vacationName = vacationRecord.getVacationName();
        this.detail = vacationRecord.getDetail();
        this.approveState = vacationRecord.getApproveState();
        this.dateCreate = vacationRecord.getDateCreate();
        this.dateUpdate = vacationRecord.getDateUpdate();

    }
        @Override
        public VacationRecordItem build() {
            return new  VacationRecordItem(this);
        }
    }
}
