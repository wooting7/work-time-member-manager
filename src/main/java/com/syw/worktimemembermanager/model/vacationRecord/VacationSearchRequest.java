package com.syw.worktimemembermanager.model.vacationRecord;

import com.syw.worktimemembermanager.enums.ApproveState;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class VacationSearchRequest {
    private LocalDate dateRequest;      //요청일자

    private ApproveState approveState;      //결재상태, like 검색



}
