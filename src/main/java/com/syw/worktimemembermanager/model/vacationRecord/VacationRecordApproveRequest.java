package com.syw.worktimemembermanager.model.vacationRecord;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationRecordApproveRequest {

}
