package com.syw.worktimemembermanager.model.vacationRecord;

import com.syw.worktimemembermanager.enums.VacationName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class VacationRecordApplyRequest {

    // 연차 등록 위한 정보
    @ApiModelProperty(notes = "시작시간")
    @NotNull
    @Length(min = 2, max = 20)
    private String timeStart;

    @ApiModelProperty(notes = "종료시간")
    @NotNull
    private String timeEnd;

    @ApiModelProperty(notes = "요청일자")
    @NotNull
    private LocalDate dateRequest;

    @ApiModelProperty(notes = "근태명")
    @NotNull
    private VacationName vacationName;

    @ApiModelProperty(notes = "상세내역")
    @NotNull
    @Length(min = 2, max = 50)
    private String detail;


}
