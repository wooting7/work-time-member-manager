package com.syw.worktimemembermanager.model.member;

import com.syw.worktimemembermanager.entity.Member;
import com.syw.worktimemembermanager.intefaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberLoginResponse {

    //로그인 기본정보 현황
    @ApiModelProperty(value = "사원시퀀스")
    private Long memberId;

    @ApiModelProperty(value = "아이디")
    private String username;

    @ApiModelProperty(value = "사원명")
    private String memberName;

    @ApiModelProperty(value = "부서")
    private String department;

    public MemberLoginResponse(MemberLoginResponseBuilder builder) {
        this.memberId = builder.memberId;
        this.username = builder.username;
        this.memberName = builder.memberName;
        this.department = builder.department;
    }

    public static class MemberLoginResponseBuilder implements CommonModelBuilder<MemberLoginResponse> {
        private final Long memberId;
        private final String username;
        private final String memberName;
        private final String department;

        public MemberLoginResponseBuilder(Member member) {
            this.memberId = member.getId();
            this.username = member.getUsername();
            this.memberName = member.getMemberName();
            this.department = member.getDepartment();
        }

        @Override
        public MemberLoginResponse build() {
            return new MemberLoginResponse(this);
        }
    }
}
