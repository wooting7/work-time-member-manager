package com.syw.worktimemembermanager.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberPasswordChangeRequest {

    @ApiModelProperty(value = "비밀번호", required = true)
    @NotNull
    @Length(min = 8, max = 30)
    private String password;

    @ApiModelProperty(value = "새 비밀번호", required = true)
    @NotNull
    @Length(min = 8, max = 30)
    private String newPassword;

    @ApiModelProperty(value = "새 비밀번호 확인", required = true)
    @NotNull
    @Length(min = 8, max = 30)
    private String newPasswordConfirm ;
}
