package com.syw.worktimemembermanager.model.member;

import com.syw.worktimemembermanager.entity.Member;
import com.syw.worktimemembermanager.intefaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MemberItem {

    // 사원의 모든정보
    @ApiModelProperty(value = "사원시퀀스")
    private Long id;

    @ApiModelProperty(notes = "아이디")
    private String username;

    @ApiModelProperty(notes = "비밀번호")
    private String password;

    @ApiModelProperty(notes = "사원명")
    private String memberName;

    @ApiModelProperty(notes = "부서")
    private String department;

    @ApiModelProperty(notes = "직책")
    private String position;

    @ApiModelProperty(notes = "생년월일")
    private LocalDate birthday;

    @ApiModelProperty(notes = "입사일")
    private LocalDate dateJoin;

    @ApiModelProperty(notes = "퇴사여부")
    private Boolean isBoolean;


    @ApiModelProperty(notes = "전화번호")
    private String phoneNumber;

    @ApiModelProperty(notes = "최종학력")
    private String finalEducation;

    @ApiModelProperty(notes = "집주소")
    private String address;

    @ApiModelProperty(notes = "비상연락망")
    private String emergencyContactNumber;

    @ApiModelProperty(notes = "비상연락망 성함")
    private String emergencyContactName;

    @ApiModelProperty(notes = "비상연락망 관계")
    private String emergencyContactRelationship;

    @ApiModelProperty(notes = "생성일")
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "수정일")
    private LocalDateTime  dateUpdate;

    public MemberItem(MemberItemBuilder builder) {
        this.id = builder.id;
        this.username = builder.username;
        this.password = builder.password;
        this.memberName = builder.memberName;
        this.department = builder.department;
        this.position = builder.position;
        this.birthday = builder.birthday;
        this.dateJoin = builder.dateJoin;
        this.isBoolean = builder.isBoolean;
        this.phoneNumber = builder.phoneNumber;
        this.finalEducation = builder.finalEducation;
        this.address = builder.address;
        this.emergencyContactNumber = builder.emergencyContactNumber;
        this.emergencyContactName = builder.emergencyContactName;
        this.emergencyContactRelationship = builder.emergencyContactRelationship;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final String username;
        private final String password;
        private final String memberName;
        private final String department;
        private final String position;
        private final LocalDate birthday;
        private final LocalDate dateJoin;
        private final Boolean isBoolean;
        private final String phoneNumber;
        private final String finalEducation;
        private final String address;
        private final String emergencyContactNumber;
        private final String emergencyContactName;
        private final String emergencyContactRelationship;
        private final LocalDateTime dateCreate;
        private final LocalDateTime  dateUpdate;

        public MemberItemBuilder(Member member) {
            this.id = member.getId();
            this.username = member.getUsername();
            this.password = member.getPassword();
            this.memberName = member.getMemberName();
            this.department = member.getDepartment();
            this.position = member.getPosition();
            this.birthday = member.getBirthday();
            this.dateJoin = member.getDateJoin();
            this.isBoolean = member.getIsBoolean();
            this.phoneNumber = member.getPhoneNumber();
            this.finalEducation = member.getFinalEducation();
            this.address = member.getAddress();
            this.emergencyContactNumber = member.getEmergencyContactNumber();
            this.emergencyContactName = member.getEmergencyContactName();
            this.emergencyContactRelationship = member.getEmergencyContactRelationship();
            this.dateCreate = member.getDateCreate();
            this.dateUpdate = member.getDateUpdate();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
