package com.syw.worktimemembermanager.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
public class MemberAddInfoRequest {

    // 사원정보 자세한 추가 입력항목
    @ApiModelProperty(notes = "전화번호", required = true)
    private String phoneNumber;

    @ApiModelProperty(notes = "최종학력", required = true)
    private String finalEducation;

    @ApiModelProperty(notes = "비상연락망", required = true)
    private String emergencyContactNumber;

    @ApiModelProperty(notes = "비상연락망 성함", required = true)
    private String emergencyContactName;

    @ApiModelProperty(notes = "비상연락망 관계", required = true)
    private String emergencyContactRelationship;

    @ApiModelProperty(notes = "집주소", required = true)
    private String address;


}
