package com.syw.worktimemembermanager.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {

    // 사원가입시 필요 정보
    @ApiModelProperty(notes = "아이디", required = true)
    @NotNull
    @Length(min =2, max =20)
    private String username;

    @ApiModelProperty(notes = "비밀번호", required = true)
    @NotNull
    @Length(min =8, max =30)
    private String password;

    @ApiModelProperty(notes = "사원명", required = true)
    @NotNull
    @Length(min =2, max =20)
    private String memberName;

    @ApiModelProperty(notes = "부서", required = true)
    @NotNull
    @Length(min =2, max =20)
    private String department;

    @ApiModelProperty(notes = "직책", required = true)
    @NotNull
    @Length(min =2, max =20)
    private String position;

    @ApiModelProperty(notes = "생년월일", required = true)
    @NotNull
    private LocalDate birthday;

    @ApiModelProperty(notes = "입사일", required = true)
    @NotNull
    private LocalDate dateJoin;




}
