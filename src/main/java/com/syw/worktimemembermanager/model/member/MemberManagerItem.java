package com.syw.worktimemembermanager.model.member;

import com.syw.worktimemembermanager.entity.Member;
import com.syw.worktimemembermanager.intefaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberManagerItem {

    @ApiModelProperty(value = "사원시퀀스")
    private Long memberId;

    @ApiModelProperty(value = "아이디")
    private String username;

    @ApiModelProperty(value = "사원명")
    private String memberName;

    @ApiModelProperty(notes = "퇴사여부")
    private Boolean isBoolean;

    @ApiModelProperty(notes = "권한등급")
    private Boolean powerGrade;

    private MemberManagerItem(MemberManagerItemBuilder builder) {
        this.memberId = builder.memberId;
        this.username = builder.username;
        this.memberName = builder.memberName;
        this.isBoolean = builder.isBoolean;
        this.powerGrade = builder.powerGrade;
    }

    public static class MemberManagerItemBuilder implements CommonModelBuilder<MemberManagerItem> {
        private final Long memberId;
        private final String username;
        private final String memberName;
        private final Boolean isBoolean;
        private final Boolean powerGrade;

        public MemberManagerItemBuilder(Member member) {
            this.memberId = member.getId();
            this.username = member.getUsername();
            this.memberName = member.getMemberName();
            this.isBoolean = member.getIsBoolean();
            this.powerGrade = member.getPowerGrade();
        }

        @Override
        public MemberManagerItem build() {
            return new MemberManagerItem(this);
        }
    }
}
