package com.syw.worktimemembermanager.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberDataRequest {

    // 사원정보중 번호만 가져오기
    @ApiModelProperty(value = "사원시퀀스")
    private Long memberId;
}
