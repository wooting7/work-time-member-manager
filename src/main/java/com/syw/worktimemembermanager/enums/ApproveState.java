package com.syw.worktimemembermanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApproveState {
    STAND_BY("대기중")
    , APPROVE("승인")
    , REJECT("반려")
    , CANCEL("회수");

    private final String name;
}
