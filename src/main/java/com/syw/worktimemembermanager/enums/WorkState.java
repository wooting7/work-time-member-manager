package com.syw.worktimemembermanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WorkState {
    COMPANY_IN("출근")
    , EARLY_LEAVE("조퇴")
    , COMPANY_OUT("퇴근")
    , NONE("상태없음");

    private final String name;
}
