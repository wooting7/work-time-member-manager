package com.syw.worktimemembermanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0, "성공하였습니다.")
    , FAILED(-1, "실패하였습니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")

    , NO_MEMBER_DATA(-20000, "사원정보가 없습니다.")
    , NO_USERNAME_DATA(-20001, "아이디가 존재하지 않습니다")
    , WRONG_PASSWORD_DATA(-20002, "잘못된 비밀번호 입니다")
    , OUT_MEMBER_DATA(-20003,"퇴사한 사원입니다. 관리자에게 문의해주세요")
    , NOT_MACH_NEW_PASSWORD(-20004, "새비밀번호와 비밀번호 확인이 일치하지 않습니다")

    , ALREADY_IN_WORK_TIME_RECORD_DATA(-30000,"이미 출근 하였습니다")
    , NO_IN_WORK_TIME_RECORD(-30001,"출근 기록이 없습니다.")
    , NOT_CHANGED_OUT_WORK_TIME_RECORD(-30002,"퇴근후에는 상태를 변경 할 수 없습니다.")
    , NOT_CHANGED_ALREADY_IN_WORK_TIME_RECORD(-30003,"근무상태를 다시 출근으로 변경 할 수 없습니다.")
    , NOT_CHANGED_SAME_WORK_TIME_RECORD(-30004,"같은 근무상태로 다시 변경 할 수 없습니다.")

    , EXIT_APPROVE_STATE(-40001, "이미 결재가 완료되었습니다")
    , NO_MINUS_VALUE(-40002, "마이너스(-) 값은 입력 할 수 없습니다")
    , NO_SAME_DATE_REQUEST(-40003, "동일한 일자에 중복하여 신청할 수 없습니다")
    , BE_SHORT_OF_VACATION_TOTAL(-40004, "연차 개수가 부족합니다")
    ;

    private final Integer code;
    private final String msg;
}
