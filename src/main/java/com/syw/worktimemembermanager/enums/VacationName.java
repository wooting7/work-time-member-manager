package com.syw.worktimemembermanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VacationName {

    HALF_VACATION("반차")
    , VACATION("연차");

    private final String name;
}
