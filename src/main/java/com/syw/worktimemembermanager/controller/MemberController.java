package com.syw.worktimemembermanager.controller;

import com.syw.worktimemembermanager.entity.Member;
import com.syw.worktimemembermanager.model.common.CommonResult;
import com.syw.worktimemembermanager.model.common.ListResult;
import com.syw.worktimemembermanager.model.common.SingleResult;
import com.syw.worktimemembermanager.model.member.*;
import com.syw.worktimemembermanager.service.MemberService;
import com.syw.worktimemembermanager.service.VacationService;
import com.syw.worktimemembermanager.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;


@Api(tags = "사원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("v1/member")
public class MemberController {
    private final MemberService memberService;

    private final VacationService vacationService;

    @ApiOperation(value = "(관리자) 사원 등록_1st")
    @PostMapping("/admin/new")
    public CommonResult setMemberAdmin(@RequestBody @Valid MemberJoinRequest request) {
        Member member = memberService.setMemberAdmin(request);              // 사원 등록
        vacationService.setVacation(member.getId(), member.getDateJoin());  // 연차 등록
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "(사용자) 사원 등록_2nd")
    @PutMapping("/user/new/{memberId}")
    public CommonResult putMemberUser(@PathVariable long memberId,@RequestBody MemberAddInfoRequest request ) {
        memberService.putMemberUser(memberId,request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "(관리자) 사원 퇴사 처리")
    @DeleteMapping("/admin/{memberId}")
    public CommonResult putWithdraw(@PathVariable long memberId,@RequestBody LocalDate dateWithdraw) {
        memberService.putWithdraw(memberId, dateWithdraw);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "(관리자) 사원 권한 변경")
    @PutMapping("/admin/powerGrade/{memberId}")
    public CommonResult putPowerGrade(@PathVariable long memberId) {
        memberService.putPowerGrade(memberId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "(관리자) 사원 비밀번호 초기화")
    @PutMapping("/admin/password/{memberId}")
    public CommonResult putSecretNumberReset(@PathVariable long memberId) {
        memberService.putSecretNumberReset(memberId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "(사용자) 사원 비밀번호 변경")
    @PutMapping("/user/password/{memberId}")
    public CommonResult putSecretNumberUser(@PathVariable long memberId, @RequestBody MemberPasswordChangeRequest changeRequest) {
        memberService.putSecretNumberUser(memberId, changeRequest);
        return ResponseService.getSuccessResult();
    }



    @ApiOperation(value =  "(사용자) 로그인")
    @PostMapping("/user/login")
    public SingleResult<MemberLoginResponse> doLogin(@RequestBody MemberLoginRequest loginRequest) {
        return ResponseService.getSingleResult(memberService.doLogin(false, loginRequest));
    }


    @ApiOperation(value = "(관리자) 로그인")
    @PostMapping("/admin/login")
    public CommonResult doLoginAdmin(@RequestBody MemberLoginRequest loginRequest) {
        memberService.doLogin(true, loginRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value =  "(관리자) 사원 정보 조회")
    @GetMapping("/admin/search")
    public ListResult<MemberManagerItem> getMembersManager() {
        return ResponseService.getListResult(memberService.getMembersManager(), true);
    }

    @ApiOperation(value =  "(사용자) 사원 정보 조회")
    @GetMapping("/user/search/member-id/{memberId}")
    public SingleResult<MemberItem> getMemberUser(@PathVariable long memberId) {
        return ResponseService.getSingleResult(memberService.getMemberUser(memberId));
    }
}
