package com.syw.worktimemembermanager.controller;

import com.syw.worktimemembermanager.entity.Member;
import com.syw.worktimemembermanager.enums.WorkState;
import com.syw.worktimemembermanager.model.common.DateSearchRequest;
import com.syw.worktimemembermanager.model.common.ListResult;
import com.syw.worktimemembermanager.model.work.WorkTimeAdminResponse;
import com.syw.worktimemembermanager.model.work.WorkTimeRecodeStatusResponse;
import com.syw.worktimemembermanager.model.common.SingleResult;
import com.syw.worktimemembermanager.model.work.WorkTimeRecordItem;
import com.syw.worktimemembermanager.service.MemberService;
import com.syw.worktimemembermanager.service.common.ResponseService;
import com.syw.worktimemembermanager.service.WorkTimeRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;


@Api(tags = "출퇴근 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("v1/work-time")
public class WorkTimeRecordController {
    private final MemberService memberService;
    private final WorkTimeRecordService workTimeRecordService;


    @ApiOperation(value = "(사용자) 사원 출퇴근 현황 조회")
    @GetMapping("/user/search/{memberId}")
    public SingleResult<WorkTimeRecodeStatusResponse> getMyStatus(@PathVariable long memberId) {
        return ResponseService.getSingleResult(workTimeRecordService.getMyStatus(memberId));
    }

    @ApiOperation(value = "(사용자) 사원 출퇴근 상태 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "workState", value = "근태 상태", required = true),
            @ApiImplicitParam(name = "memberId", value = "사원 시퀀스", required = true),
    })
    @PutMapping("/user/status/{workState}/member-id/{memberId}")
    public SingleResult<WorkTimeRecodeStatusResponse> doStatusChange(
            @PathVariable WorkState workState,
            @PathVariable long memberId
    ) {
        Member member = memberService.getMemberData(memberId);
        return ResponseService.getSingleResult(workTimeRecordService.doWorkTimeRecodeChange(member, workState));
    }


    @ApiOperation(value =  "(관리자) 사원 출퇴근기록 일일 조회")
    @GetMapping("/admin/search")
    public ListResult<WorkTimeAdminResponse> getWorkTimeAdmin(@RequestParam(value = "근무일자") @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateWork) {
        return ResponseService.getListResult(workTimeRecordService.getWorkTimeAdmin(dateWork), true);
    }


    @ApiOperation(value = "(사용자) 출퇴근기록 월별 조회")
    @PostMapping("/user/search")
    public ListResult<WorkTimeRecordItem> getWorkTimeListUser(@RequestBody @Valid DateSearchRequest searchRequest) {
        return ResponseService.getListResult(workTimeRecordService.getWorkTimeListUser(searchRequest), true);
    }



}
