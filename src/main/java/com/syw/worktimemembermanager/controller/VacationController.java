package com.syw.worktimemembermanager.controller;

import com.syw.worktimemembermanager.entity.Member;
import com.syw.worktimemembermanager.enums.ApproveState;
import com.syw.worktimemembermanager.model.common.DateSearchRequest;
import com.syw.worktimemembermanager.model.common.ListResult;
import com.syw.worktimemembermanager.model.vacationRecord.VacationRecordApplyRequest;
import com.syw.worktimemembermanager.model.common.CommonResult;
import com.syw.worktimemembermanager.model.vacationRecord.VacationRecordItem;
import com.syw.worktimemembermanager.service.MemberService;
import com.syw.worktimemembermanager.service.VacationService;
import com.syw.worktimemembermanager.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "연차 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("v1/vacation")
public class VacationController {
    private final MemberService memberService;

    private final VacationService vacationService;

    @ApiOperation(value = "(사용자) 연차 등록")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "사원 시퀀스", required = true)
    })
    @PostMapping("/user/new/member-id/{memberId}")
    public CommonResult setVacationApply(@PathVariable long memberId, @RequestBody @Valid VacationRecordApplyRequest applyRequest) {
        Member member = memberService.getMemberData(memberId);
        vacationService.setVacationApply(member, applyRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "(관리자) 연차 결재")
    @ApiImplicitParams({
            @ApiImplicitParam(name ="vacationRecordId", value = "연차 기록 시퀀스", required = true),
            @ApiImplicitParam(name = "approveState", value = "결재상태", required = true)
    })
    @PutMapping("/admin/vacation-id/{vacationRecordId}/approve-state/{approveState}")
    public CommonResult putVacationApply(@PathVariable long vacationRecordId,@PathVariable ApproveState approveState) {
        vacationService.putVacationApply(approveState, vacationRecordId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "(관리자) 연차 부여")
    @ApiImplicitParams({
            @ApiImplicitParam(name ="memberId", value = "총 휴가 개수 시퀀스", required = true),
            @ApiImplicitParam(name = "increaseValue", value = "증가값", required = true)
    })
    @PutMapping("/admin/vacation-count/{memberId}")
    public CommonResult putVacationCount(@PathVariable long memberId, float increaseValue) {
        vacationService.putVacationCount(memberId, increaseValue);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "(관리자) 연차 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "페이지 숫자", required = true),
            @ApiImplicitParam(name = "dateRequest", value = "요청일", required = true)
    })
    @GetMapping("/admin/page/{pageNum}")
    public ListResult<VacationRecordItem> getVacation(
            @PathVariable int pageNum,
            @RequestParam(value = "dateRequest") @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateRequest) {
        return ResponseService.getListResult(vacationService.getVacationRecordList(pageNum, dateRequest ), true);
    }


    @ApiOperation(value = "(사용자) 연차 조회")
    @PostMapping("/user/search")
    public ListResult<VacationRecordItem> getVacationUser(@RequestBody @Valid DateSearchRequest searchRequest) {
        return ResponseService.getListResult(vacationService.getVacationRecordListUser(searchRequest), true);
    }


}

