package com.syw.worktimemembermanager.service;

import com.syw.worktimemembermanager.entity.Member;
import com.syw.worktimemembermanager.exception.*;
import com.syw.worktimemembermanager.model.common.ListResult;
import com.syw.worktimemembermanager.model.member.*;
import com.syw.worktimemembermanager.repository.MemberRepository;
import com.syw.worktimemembermanager.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;


    /**
     * (관리자) 사원 등록
     * @param request 사원 등록에 필요한 정보
     * @return 사원 정보
     */
    public Member setMemberAdmin(MemberJoinRequest request) {
        Member member = new Member.MemberBuilder(request).build();
        return memberRepository.save(member);
        //저장 후 저장한 결과값(엔티티모양 전체)을 바로 반환 한다는 것이 포인트.
    }


    /**
     * (사용자) 사원 등록
     * @param id 사원 시퀀스,
     * @param request 사원 등록에 추가로 필요한 정보
     */
    public void putMemberUser(long id, MemberAddInfoRequest request) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.setMemberUser(request);
        memberRepository.save(member);
    }

    /**
     * (관리자) 사원 퇴사 처리
     * @param id 사원 시퀀스
     * @param dateWithdraw 퇴사일
     */
    public void putWithdraw(long id, LocalDate dateWithdraw) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putWithdraw(dateWithdraw);
        memberRepository.save(member);
    }

    /**
     * (관리자) 사원 권한 변경
     * @param id 사원 시퀀스
     */
    public void putPowerGrade(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putPowerGrade();
        memberRepository.save(member);
    }

    /**
     * (관리자) 사원 비밀번호 초기화
     * @param id 사원 시퀀스
     */
    public void putSecretNumberReset(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putSecretNumberAdmin();
        memberRepository.save(member);
    }

    /**
     * (사용자) 사원 비밀번호 변경
     * @param memberId 사원 시퀀스
     * @param changeRequest 현재비번, 변경할비번
     */
    public void putSecretNumberUser(long memberId, MemberPasswordChangeRequest changeRequest) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        if (!member.getPassword().equals(changeRequest.getPassword())) throw new CWrongPasswordDataException();
        // 잘못된 비밀번호 입니다
        if (!changeRequest.getNewPassword().equals(changeRequest.getNewPasswordConfirm())) throw new CNotMachNewPasswordException();
        // 새비밀번호와 비밀번호 확인이 일치하지 않습니다
        member.putSecretNumberUser(changeRequest);
        memberRepository.save(member);
    }


    /**
     * (공용) 로그인 메서드
     * @param powerGrade 관리자권한
     * @param loginRequest 아이디,비밀번호
     * @return 사원 간략정보
     */
    public MemberLoginResponse doLogin(boolean powerGrade, MemberLoginRequest loginRequest) {
        Member member = memberRepository.findByUsernameAndPowerGrade(loginRequest.getUsername(), powerGrade).orElseThrow(CNoUsernameDataException::new);
        // "아이디가 존재하지 않습니다"

        if (!member.getPassword().equals(loginRequest.getPassword())) throw new CWrongPasswordDataException();
        // "잘못된 비밀번호 입니다"

        if (!member.getIsBoolean()) throw new COutMemberDataException();
        // "퇴사한 사원입니다. 관리자에게 문의해주세요"

        return new MemberLoginResponse.MemberLoginResponseBuilder(member).build();
    }

    /**
     * (공용) 사원정보의 시퀀스 가져오는 메서드
     * @param id 사원 시퀀스
     * @return 사원정보
     */
    public Member getMemberData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        // "데이터를 찾을 수 없습니다."
    }

    /**
     * (관리자) 사원 정보 조회
     * @return 모든 사원의 정보 조회
     */
    public ListResult<MemberManagerItem> getMembersManager() {

        List<Member> members = memberRepository.findAll();

        List<MemberManagerItem> result = new LinkedList<>();
        members.forEach(member -> {
        MemberManagerItem addItem = new MemberManagerItem.MemberManagerItemBuilder(member).build();
        result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * (사용자) 사원 정보 조회
     * @param memberId 사원 시퀀스
     * @return 사원의 정보 조회
     */
    public MemberItem getMemberUser (long memberId) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        return new MemberItem.MemberItemBuilder(member).build();
    }

}
