package com.syw.worktimemembermanager.service;


import com.syw.worktimemembermanager.entity.Member;
import com.syw.worktimemembermanager.entity.WorkTimeRecord;
import com.syw.worktimemembermanager.enums.WorkState;
import com.syw.worktimemembermanager.exception.*;
import com.syw.worktimemembermanager.model.common.DateSearchRequest;
import com.syw.worktimemembermanager.model.common.ListResult;
import com.syw.worktimemembermanager.model.work.WorkTimeAdminResponse;
import com.syw.worktimemembermanager.model.work.WorkTimeRecodeStatusResponse;
import com.syw.worktimemembermanager.model.work.WorkTimeRecordItem;
import com.syw.worktimemembermanager.repository.WorkTimeRecordRepository;
import com.syw.worktimemembermanager.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class WorkTimeRecordService {
    private final WorkTimeRecordRepository workTimeRecordRepository;


    /**
     * (사용자) 사원 출퇴근 현황 조회
     * @param memberId 사원시퀀스
     * @return 당일 사원의 출퇴근정보
     */
    public WorkTimeRecodeStatusResponse getMyStatus(long memberId) {
        WorkTimeRecord workTimeRecord = workTimeRecordRepository
                .findByDateWorkAndMember_Id(LocalDate.now(), memberId)
                .orElse(new WorkTimeRecord.WorkTimeRecordNoneValueBuilder().build());
       return new WorkTimeRecodeStatusResponse.WorkTimeRecodeStatusResponseBuilder(workTimeRecord).build();
    }

    /**
     * (사용자) 사원 출퇴근 상태 수정
     * @param member 사원정보
     * @param workState 결재상태
     * @return 당일 사원의 출퇴근정보
     */
    public WorkTimeRecodeStatusResponse doWorkTimeRecodeChange(Member member, WorkState workState) {
        Optional<WorkTimeRecord> workTimeRecord = workTimeRecordRepository.findByDateWorkAndMember_Id(LocalDate.now(), member.getId());

        WorkTimeRecord workTimeRecordResult;
        if (workTimeRecord.isEmpty()) workTimeRecordResult = setWorkTimeRecord(member);
        else workTimeRecordResult = putWorkTimeRecord(workTimeRecord.get(), workState);

        return new WorkTimeRecodeStatusResponse.WorkTimeRecodeStatusResponseBuilder(workTimeRecordResult).build();
    }

    /**
     * (사용자) 사원 출근 등록
     * @param member 사원정보
     * @return 출근등록
     */
    private WorkTimeRecord setWorkTimeRecord(Member member) {
        WorkTimeRecord data = new WorkTimeRecord.WorkTimeRecordBuilder(member).build();
        return workTimeRecordRepository.save(data);
    }

    /**
     * (사용자) 사원 조퇴/퇴근 등록
     * @param workTimeRecord 출퇴근기록
     * @param workState 출퇴근상태
     * @return 사원 조퇴/퇴근 등록
     */
    private WorkTimeRecord putWorkTimeRecord(WorkTimeRecord workTimeRecord, WorkState workState) {
        // 이미 출근 하였습니다
        if (workState.equals(WorkState.COMPANY_IN)) throw new CAlreadyInWorkTimeRecordDataException();
        // 퇴근 후에는 상태를 변경 할수 없습니다
        if (workTimeRecord.getWorkState().equals(WorkState.COMPANY_OUT)) throw new CNotChangedOutWorkTimeRecord();
        // 퇴근 후에는 상태를 변경 할수 없습니다
        if (workTimeRecord.getWorkState().equals(WorkState.EARLY_LEAVE)) throw new CNotChangedOutWorkTimeRecord();
        // 같은 상태로 변경 할 수 없습니다
        if (workTimeRecord.getWorkState().equals(workState)) throw new CNotChangedSameWorkTimeRecord();


        workTimeRecord.putWorkSate(workState);
        return workTimeRecordRepository.save(workTimeRecord);
    }

    /**
     * (관리자) 사원 근태기록 일일 조회
     * @param dateWork 근무일
     * @return 당일 모든 사원의 근태기록
     */
    public ListResult<WorkTimeAdminResponse> getWorkTimeAdmin(LocalDate dateWork) {
        List<WorkTimeRecord> workTimeRecords= workTimeRecordRepository.findAllByDateWork(dateWork);

        List<WorkTimeAdminResponse> result = new LinkedList<>();

        workTimeRecords.forEach(workTimeRecord -> {
        WorkTimeAdminResponse addItem = new WorkTimeAdminResponse.MemberAdminResponseBuilder(workTimeRecord).build();
        result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

    /**
     * (사용자) 출퇴근 조회
     * @param searchRequest 사원시퀀스, 년도, 월
     * @return 사원의 달별 출퇴흔 리스트
     */
    public ListResult<WorkTimeRecordItem> getWorkTimeListUser(DateSearchRequest searchRequest) {
        LocalDate dateStart = LocalDate.of(searchRequest.getDateStandardYear(), searchRequest.getDateStandardMonth(), 1);
        Calendar calendar = Calendar.getInstance();
        calendar.set(searchRequest.getDateStandardYear(), searchRequest.getDateStandardMonth() -1,1);
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, maxDay);
        LocalDate dateEnd = LocalDate.of(searchRequest.getDateStandardYear(), searchRequest.getDateStandardMonth(), maxDay);

        List<WorkTimeRecord> workTimeRecords = workTimeRecordRepository.findAllByMember_IdAndDateWorkGreaterThanEqualAndDateWorkLessThanEqual(searchRequest.getMemberId(), dateStart, dateEnd);
        List<WorkTimeRecordItem> result = new LinkedList<>();

        workTimeRecords.forEach(workTimeRecord -> {
            WorkTimeRecordItem item = new WorkTimeRecordItem.WorkTimeRecordItemBuilder(workTimeRecord).build();
            result.add(item);
        });
        return ListConvertService.settingResult(result);
    }
}
