package com.syw.worktimemembermanager.service;

import com.syw.worktimemembermanager.entity.Member;
import com.syw.worktimemembermanager.entity.VacationCount;
import com.syw.worktimemembermanager.entity.VacationRecord;
import com.syw.worktimemembermanager.enums.ApproveState;
import com.syw.worktimemembermanager.enums.VacationName;
import com.syw.worktimemembermanager.exception.CBeShortOfVacationTotalException;
import com.syw.worktimemembermanager.exception.CExitApproveState;
import com.syw.worktimemembermanager.exception.CMissingDataException;
import com.syw.worktimemembermanager.exception.CNoSameDateRequestException;
import com.syw.worktimemembermanager.model.common.ListResult;
import com.syw.worktimemembermanager.model.vacationRecord.VacationRecordItem;
import com.syw.worktimemembermanager.model.vacationRecord.VacationRecordApplyRequest;
import com.syw.worktimemembermanager.model.common.DateSearchRequest;
import com.syw.worktimemembermanager.repository.VacationCountRepository;
import com.syw.worktimemembermanager.repository.VacationRecordRepository;
import com.syw.worktimemembermanager.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VacationService {

    @PersistenceContext
    EntityManager entityManager;
    private final VacationRecordRepository vacationRecordRepository;

    private final VacationCountRepository vacationCountRepository;

    /**
     * (관리자) 사원가입시 연차등록 메서드
     * @param memberId 사원 시퀀스
     * @param dateJoin 입사일
     */
    public void setVacation(long memberId, LocalDate dateJoin) {
        VacationCount vacationCount = new VacationCount.VacationCountBuilder(memberId, dateJoin).build();
        vacationCountRepository.save(vacationCount);
    }

    /**
     * (사용자) 연차 등록
     * @param member 사원정보
     * @param applyRequest 연차에 필요한 정보
     */
    public void setVacationApply(Member member, VacationRecordApplyRequest applyRequest) {
        if (vacationRecordRepository.countByDateRequestAndMember_Id(applyRequest.getDateRequest(), member.getId()) >= 1) throw new CNoSameDateRequestException();
        //  동일한 일자에 중복하여 신청할 수 없습니다
        VacationRecord vacationRecord = new VacationRecord.VacationRecordBuilder(member, applyRequest).build();
        VacationCount vacationCount = vacationCountRepository.findByMemberId(member.getId()).orElseThrow(CMissingDataException::new);

        float plusValue = vacationRecord.getVacationName() == VacationName.VACATION ? 1f : 0.5f;

        if (plusValue > vacationCount.getCountRest()) throw new CBeShortOfVacationTotalException();    //  연차 개수가 부족합니다.


        vacationRecordRepository.save(vacationRecord);
    }


    /**
     * (관리자) 연차 결재
     * @param approveState 결제상태
     * @param vacationRecordId 연차기록 시퀀스
     */
    public void putVacationApply(ApproveState approveState, long vacationRecordId) {
        VacationRecord vacationRecord = vacationRecordRepository.findById(vacationRecordId).orElseThrow(CMissingDataException::new);        //근태신청기록 가져와

        if (vacationRecord.getApproveState() != ApproveState.STAND_BY)  throw new CExitApproveState();       // 이미 결재가 완료되었습니다
        if (vacationRecordRepository.countByDateRequestAndMember_Id(vacationRecord.getDateRequest(), vacationRecord.getMember().getId()) > 1) throw new CNoSameDateRequestException();
        //  동일한 일자에 중복하여 신청할 수 없습니다

        vacationRecord.putVacationApply(approveState);      // 근태신청기록에 연차&반차 증가값 입력해,반려면은 시간만 반환해

        if (vacationRecord.getApproveState().equals(ApproveState.APPROVE)) {        //승인일때
            VacationCount vacationCount = vacationCountRepository.findByMemberId(vacationRecord.getMember().getId()).orElseThrow(CMissingDataException::new);   //총연차 개수기록 가져와
            vacationCount.putUseVacation(vacationRecord.getIncreaseValue());            // 사용 연차개수 + 등록

            if (vacationRecord.getIncreaseValue() > vacationCount.getCountRest()) throw new CBeShortOfVacationTotalException();    //  연차 개수가 부족합니다.
            vacationCountRepository.save(vacationCount);                                // 사용 연차개수 등록한거 저장해
            }
        vacationRecordRepository.save(vacationRecord);            // 근태신청기록 저장해
    }

    /**
     * (관리자) 연차 부여
     * @param memberId 사원시퀀스
     * @param increaseValue 증가값
     */
    public void putVacationCount(long memberId, float increaseValue) {
        VacationCount vacationCount = vacationCountRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        vacationCount.plusCountTotal(increaseValue);
        vacationCountRepository.save(vacationCount);
    }

    /**
     * (관리자) 연차 조회
     * @param pageNum n번째 페이지
     * @param dateRequest 요청일
     * @return
     */
    public ListResult<VacationRecordItem> getVacationRecordList(int pageNum, LocalDate dateRequest) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        Page<VacationRecord> vacationRecords = getData(pageRequest, dateRequest);

        ListResult<VacationRecordItem> result = new ListResult<>();
        result.setTotalItemCount(vacationRecords.getTotalElements());
        result.setTotalPage(vacationRecords.getTotalPages() == 0 ? 1 : vacationRecords.getTotalPages());
        result.setCurrentPage(vacationRecords.getPageable().getPageNumber() + 1);

        List<VacationRecordItem> list = new LinkedList<>();
        vacationRecords.forEach(vacationRecord -> list.add(new VacationRecordItem.VacationItemBuilder(vacationRecord).build()));
        result.setList(list);

        return result;
    }

    private Page<VacationRecord> getData(Pageable pageable, LocalDate dateRequest) {
        // Criteria 라는 단어 자체가 기준, 조건 이라는 뜻임.
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder(); // 조건을 넣을 수 있게 해주는 빌더를 가져옴.
        CriteriaQuery<VacationRecord> criteriaQuery = criteriaBuilder.createQuery(VacationRecord.class); // 기본 쿼리 생성(member select)

        // ※ 실제로는 여기에 기재된 엔티티들만 잘 설정하면 됨. ※
        Root<VacationRecord> root = criteriaQuery.from(VacationRecord.class); // 대장 엔티티를 설정해 줌.

        List<Predicate> predicates = new LinkedList<>(); // 검색 조건들을 넣을 리스트를 생성함.
        // ※ 실제로는 여기에 기재된 검색 조건들만 잘 기재하면 됨. ※
        // StringUtils.length 를 쓰는 이유. (build.gradle에 새로운 패키지 추가해놨음.)
        // String에 글자수를 가져와라 라고 length 를 쓰게 되면 null값의 경우 에러를 내뱉음. (nullpoint exception)
        // 하지만 common-lang3 패키지의 StringUtils.length 를 쓰게 되면 null값을 넣어도 에러를 뱉지 않고 숫자 0을 줌.


        // 영어단어를 잘 읽고 아래 조건들은 말로 해석하면 됨.
        predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("dateRequest"), dateRequest.withDayOfMonth(1)));
        predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("dateRequest"), dateRequest.withDayOfMonth(dateRequest.lengthOfMonth())));
        // ※ 실제로는 여기까지 기재된 검색 조건들만 잘 기재하면 됨. ※

        Predicate[] predArray = new Predicate[predicates.size()]; // 총 where 조건의 갯수를 구함.
        predicates.toArray(predArray); // 리스트를 배열로 변환
        criteriaQuery.where(predArray); // 기본쿼리에 where 문을 생성해서 붙임.

        TypedQuery<VacationRecord> query = entityManager.createQuery(criteriaQuery); // 데이터베이스에 쿼리를 날림.

        int totalRows = query.getResultList().size(); // 총 데이터 개수를 가져옴.

        // 사용자한테 제공 될 결과데이터들을 가져옴.
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize()); // 데이터를 가져오는데 시작지점
        query.setMaxResults(pageable.getPageSize()); // 데이터를 가져오는데 종료지점

        return new PageImpl<>(query.getResultList(), pageable, totalRows); // 페이징을 구현한 데이터를 반환함. (현재 선택된 페이지의 데이터들, 페이징 객체, 총 데이터 수)
    }


    /**
     * (사용자) 연차 조회
     * @param searchRequest 사원시퀀스, 년도, 월
     * @return 사원의 연차등록한 리스트
     */
    public ListResult<VacationRecordItem> getVacationRecordListUser(DateSearchRequest searchRequest) {
        LocalDate dateStart = LocalDate.of(searchRequest.getDateStandardYear(), searchRequest.getDateStandardMonth(), 1);
        Calendar calendar = Calendar.getInstance();
        calendar.set(searchRequest.getDateStandardYear(), searchRequest.getDateStandardMonth() -1,1);
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, maxDay);
        LocalDate dateEnd = LocalDate.of(searchRequest.getDateStandardYear(), searchRequest.getDateStandardMonth(), maxDay);

        List<VacationRecord> vacationRecords = vacationRecordRepository.findAllByMember_IdAndDateRequestGreaterThanEqualAndDateRequestLessThanEqual(searchRequest.getMemberId(), dateStart, dateEnd);
        List<VacationRecordItem> result = new LinkedList<>();

        vacationRecords.forEach(vacationRecord -> {
            VacationRecordItem item = new VacationRecordItem.VacationItemBuilder(vacationRecord).build();
            result.add(item);
        });
        return ListConvertService.settingResult(result);
    }




}


