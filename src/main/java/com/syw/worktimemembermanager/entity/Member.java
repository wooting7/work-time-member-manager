package com.syw.worktimemembermanager.entity;

import com.syw.worktimemembermanager.intefaces.CommonModelBuilder;
import com.syw.worktimemembermanager.model.member.MemberAddInfoRequest;
import com.syw.worktimemembermanager.model.member.MemberJoinRequest;
import com.syw.worktimemembermanager.model.member.MemberPasswordChangeRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;        //사원시퀀스

    @Column(nullable = false, length = 20, unique = true)
    private String username;      //아이디

    @Column(nullable = false, length = 30)
    private String password;        //비밀번호

    @Column(nullable = false, length = 20)
    private String memberName;      //사원명

    @Column(nullable = false, length = 20)
    private String department;      //부서

    @Column(nullable = false, length = 20)
    private String position;        //직책

    @Column(nullable = false)
    private LocalDate birthday;     //생년월일

    @Column(nullable = false)
    private LocalDate dateJoin;     //입사일

    @Column
    private LocalDate dateWithdraw;     //퇴사일

    @Column(nullable = false)
    private Boolean isBoolean;      //퇴사여부

    @Column(nullable = false)
    private Boolean powerGrade;     //권한등급



    @Column(length = 30)
    private String phoneNumber;     //전화번호

    @Column(length = 10)
    private String finalEducation;      //최종학력

    @Column(length = 50)
    private String address;     //집주소

    @Column(length = 30)
    private String emergencyContactNumber;      //비상연락망

    @Column(length = 20)
    private String emergencyContactName;        //비상연락망 성함

    @Column(length = 10)
    private String emergencyContactRelationship;        //비상연락망 관계

    @Column(nullable = false)
    private LocalDateTime dateCreate;       //생성일

    @Column
    private LocalDateTime  dateUpdate;      //수정일

    public void putWithdraw(LocalDate dateWithdraw) {
        this.isBoolean = false;
        this.dateWithdraw = dateWithdraw;
        this.dateUpdate = LocalDateTime.now();
    }

    public void putPowerGrade() {
        this.powerGrade = true;
        this.dateUpdate = LocalDateTime.now();
    }

    public void putSecretNumberUser(MemberPasswordChangeRequest changeRequest) {
        this.password = changeRequest.getNewPassword();
        this.dateUpdate = LocalDateTime.now();
    }

    //생년월일로 비밀번호 초기화
    public void putSecretNumberAdmin() {
        this.password = String.format("%1$tY%1$tm%1$td", this.birthday);
        this.dateUpdate = LocalDateTime.now();
    }

    public void setMemberUser(MemberAddInfoRequest request) {
        this.phoneNumber = request.getPhoneNumber();
        this.finalEducation = request.getFinalEducation();
        this.address = request.getAddress();
        this.emergencyContactNumber = request.getEmergencyContactNumber();
        this.emergencyContactName = request.getEmergencyContactName();
        this.emergencyContactRelationship = request.getEmergencyContactRelationship();
        this.dateUpdate = LocalDateTime.now();

    }

    private Member(MemberBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
        this.memberName = builder.memberName;
        this.department = builder.department;
        this.position = builder.position;
        this.birthday = builder.birthday;
        this.dateJoin = builder.dateJoin;
        this.isBoolean = builder.isBoolean;
        this.powerGrade = builder.powerGrade;
        this.dateCreate = builder.dateCreate;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final String username;
        private final String password;
        private final String memberName;
        private final String department;
        private final String position;
        private final LocalDate birthday;
        private final LocalDate dateJoin;
        private final Boolean isBoolean;
        private final Boolean powerGrade;

        private final LocalDateTime dateCreate;

        public MemberBuilder(MemberJoinRequest request) {
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.memberName = request.getMemberName();
            this.department = request.getDepartment();
            this.position = request.getPosition();
            this.birthday = request.getBirthday();
            this.dateJoin = request.getDateJoin();
            this.isBoolean = true;
            this.powerGrade = false;
            this.dateCreate = LocalDateTime.now();

        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }

}
