package com.syw.worktimemembermanager.entity;

import com.syw.worktimemembermanager.enums.WorkState;
import com.syw.worktimemembermanager.intefaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkTimeRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId",nullable = false)
    private Member member;  //사원

    @Column(nullable = false)
    private LocalDate dateWork;     //근무일자

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 20)
    private WorkState workState;        //근태상태

    @Column(nullable = false)
    private LocalTime dateStart;        //출근시간

    private LocalTime dateEarlyLeave;       //조퇴시간

    private LocalTime dateEnd;      //퇴근시간


    @Column(nullable = false)
    private LocalDateTime dateCreate;       //생성일

    @Column
    private LocalDateTime dateUpdate;       //수정일

    public void putWorkSate(WorkState workState) {
        this.workState = workState;
        this.dateUpdate = LocalDateTime.now();

        switch (workState) {
            case EARLY_LEAVE:
                this.dateEarlyLeave = LocalTime.now();
                break;
            case COMPANY_OUT:
                this.dateEnd = LocalTime.now();
                break;
        }
    }


    public WorkTimeRecord(WorkTimeRecordBuilder builder) {
        this.member = builder.member;
        this.dateWork = builder.dateWork;
        this.workState = builder.workState;
        this.dateStart = builder.dateStart;
        this.dateCreate = builder.dateCreate;
    }
    // 출근등록
    public static class WorkTimeRecordBuilder implements CommonModelBuilder<WorkTimeRecord> {
        private final Member member;
        private final LocalDate dateWork;
        private final WorkState workState;
        private final LocalTime dateStart;
        private final LocalDateTime dateCreate;

        public WorkTimeRecordBuilder(Member member) {
            this.member = member;
            this.dateWork = LocalDate.now();
            this.workState = WorkState.COMPANY_IN;
            this.dateStart = LocalTime.now();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public WorkTimeRecord build() {
            return new WorkTimeRecord(this);
        }
    }

    // 상태없음으로 변경
    private WorkTimeRecord(WorkTimeRecordNoneValueBuilder builder) {
        this.workState = builder.workState;

    }

    public static class WorkTimeRecordNoneValueBuilder implements CommonModelBuilder<WorkTimeRecord> {
        private final WorkState workState;


        public WorkTimeRecordNoneValueBuilder() {
            this.workState = WorkState.NONE;

        }

        @Override
        public WorkTimeRecord build() {
            return new WorkTimeRecord(this);
        }
    }







}
