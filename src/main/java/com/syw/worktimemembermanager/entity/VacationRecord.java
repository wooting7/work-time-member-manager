package com.syw.worktimemembermanager.entity;

import com.syw.worktimemembermanager.enums.ApproveState;
import com.syw.worktimemembermanager.enums.VacationName;
import com.syw.worktimemembermanager.intefaces.CommonModelBuilder;
import com.syw.worktimemembermanager.model.vacationRecord.VacationRecordApplyRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId",nullable = false)
    private Member member;      //사원

    @Column(nullable = false, length = 10)
    private String timeStart;       //시작시간

    @Column(nullable = false, length = 10)
    private String timeEnd;     //종료시간

    @Column(nullable = false)
    private LocalDate dateRequest;      //요청일자

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 23)
    private VacationName vacationName;      //근태명

    @Column(nullable = false, length = 50)
    private String detail;      //상세내역

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 8)
    private ApproveState approveState;      //결재상태

    private LocalDateTime  dateApprove;     //결재시간(회수,반려,승인)

    @Column(nullable = false)
    private Boolean isMinus;        //차감여부(true면 연차 사용, false면 총 연차 개수 증가)

    @Column(nullable = false)
    private Float increaseValue;        //증가값



    @Column(nullable = false)
    private LocalDateTime dateCreate;       //생성일


    private LocalDateTime  dateUpdate;      //수정일


    public void putVacationApply(ApproveState approveState) {
        this.approveState = approveState;
        this.dateApprove = LocalDateTime.now();
        this.dateUpdate = LocalDateTime.now();

        // 연차면 1, 반차면 0.5
        switch (approveState) {
            case APPROVE:
                if (vacationName.equals(VacationName.VACATION)) {
                    this.increaseValue = 1f;
                } else if (vacationName.equals(VacationName.HALF_VACATION)) {
                    this.increaseValue = 0.5f;
                }
                break;
            case REJECT:
                break;
        }
    }


    private VacationRecord(VacationRecordBuilder builder) {
        this.member = builder.member;
        this.timeStart = builder.timeStart;
        this.timeEnd = builder.timeEnd;
        this.dateRequest = builder.dateRequest;
        this.vacationName = builder.vacationName;
        this.detail = builder.detail;
        this.approveState = builder.approveState;
        this.isMinus = builder.isMinus;
        this.increaseValue = builder.increaseValue;
        this.dateCreate = builder.dateCreate;
    }

    public static class VacationRecordBuilder implements CommonModelBuilder<VacationRecord> {
        private final Member member;
        private final String timeStart;
        private final String timeEnd;
        private final LocalDate dateRequest;
        private final VacationName vacationName;
        private final String detail;
        private final ApproveState approveState;
        private final Boolean isMinus;
        private final Float increaseValue;
        private final LocalDateTime dateCreate;

        public VacationRecordBuilder(Member member, VacationRecordApplyRequest applyRequest) {
            this.member = member;
            this.timeStart = applyRequest.getTimeStart();
            this.timeEnd = applyRequest.getTimeEnd();
            this.dateRequest = applyRequest.getDateRequest();
            this.vacationName = applyRequest.getVacationName();
            this.detail = applyRequest.getDetail();
            this.approveState = ApproveState.STAND_BY;
            this.isMinus = true;
            this.increaseValue = 0f;
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public VacationRecord build() {
            return new VacationRecord(this);
        }
    }

}
