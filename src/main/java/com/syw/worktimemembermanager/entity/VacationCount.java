package com.syw.worktimemembermanager.entity;

import com.syw.worktimemembermanager.intefaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationCount {
    @Id
    private Long memberId;

    @Column(nullable = false)
    private Float totalVacation;        // 총휴가일

    @Column(nullable = false)
    private Float useVacation;      // 사용 휴가일

    @Column(nullable = false)
    private LocalDate dateVacationCountStart;         // 연차 갯수 유효 시작일

    @Column(nullable = false)
    private LocalDate dateVacationCountEnd;     //연차 갯수 유효 종료일

    @Column(nullable = false)
    private LocalDateTime dateCreate;       //생성일

    private LocalDateTime  dateUpdate;      //수정일

    // 잔여 연차 개수
    public float getCountRest() {
        return this.totalVacation - this.useVacation;
    }

    // 사용 연차개수 증가
    public void putUseVacation(float plusCount) {
        this.useVacation += plusCount;
        this.dateUpdate = LocalDateTime.now();
    }

  // 총 연차개수 증가
    public void plusCountTotal(float plusCount) {
        this.totalVacation += plusCount;
        this.dateUpdate = LocalDateTime.now();
    }

    private VacationCount(VacationCountBuilder builder) {
        this.memberId = builder.memberId;
        this.totalVacation = builder.totalVacation;
        this.useVacation = builder.useVacation;
        this.dateVacationCountStart = builder.dateVacationCountStart;
        this.dateVacationCountEnd = builder.dateVacationCountEnd;
        this.dateCreate = builder.dateCreate;
    }

    public static class VacationCountBuilder implements CommonModelBuilder<VacationCount> {
        private final Long memberId;
        private final Float totalVacation;
        private final Float useVacation;
        private final LocalDate dateVacationCountStart;
        private final LocalDate dateVacationCountEnd;
        private final LocalDateTime dateCreate;

        public VacationCountBuilder(Long memberId, LocalDate dateJoin) {
            this.memberId = memberId;
            this.totalVacation = 0f;
            this.useVacation = 0f;
            this.dateVacationCountStart = dateJoin;
            this.dateVacationCountEnd = dateJoin.plusYears(1);
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public VacationCount build() {
            return new VacationCount(this);
        }
    }

    private VacationCount(VacationCountUseBuilder builder) {
        this.useVacation += builder.useVacation;
    }

    public static class VacationCountUseBuilder implements CommonModelBuilder<VacationCount> {
        private final Float useVacation;

        public VacationCountUseBuilder(VacationCount vacationCount) {
            this.useVacation = vacationCount.useVacation;
        }

        @Override
        public VacationCount build() {
            return null;
        }
    }
}
