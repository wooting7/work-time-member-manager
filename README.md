# 사원관리 API
***
소개: 직원들의 출퇴근 관리를 위한 API 입니다
***
#### * 목차 *
```
1. 언어
2. 기능
3. API
4. 설계
```
#### 1. 언어
```
JAVA 16
SpringBoot 2.7.2
```
#### 2. 기능
```
* 사원관리
  - (관리자) 사원 등록_1st
  - (사용자) 사원 등록_2nd
  - (관리자) 사원 퇴사 변경
  - (관리자) 사원 권한 변경
  - (관리자) 사원 비밀번호 초기화
  - (사용자) 사원 비밀번호 변경
  - (사용자) 로그인 
  - (관리자) 로그인 
  - (관리자) 사원 정보 조회 
  - (사용자) 사원 정보 조회
    
* 연차관리
  - (사용자) 연차 등록
  - (관리자) 연차 결재
  - (관리자) 연차 부여
  - (관리자) 연차 조회
  - (사용자) 연차 조회
  
* 출퇴근관리
  - (사용자) 사원 출퇴근 현황 조회
  - (사용자) 사원 출퇴근 상태 수정
  - (사용자) 사원 출퇴근 월별 조회
  - (관리자) 사원 출퇴근 일일 기록 조회
  
```

#### 3. API

>* 전체화면
> 
>![swagger_all](./images/swagger_all.png)

>* 사원관리
>
>![swagger_member](./images/swagger_member.png)
>

>* 연차관리
>
>![swagger_vacation](./images/swagger_vacation.png)

>* 출퇴근관리
>
>![swagger_work_time](./images/swagger_work_time.png)

#### 4. 설계
>* 사원관리
>
>![entity_plan_member_manager](./images/entity_plan_1.png)

>* 출퇴근기록
>
>![entity_plan_work_time_record](./images/entity_plan_2.png)

>* 근태 기록
>
>![entity_plan_vacation_count](./images/entity_plan_3.png)

>* 휴가 기록
>
>![entity_plan_vacation_list](./images/entity_plan_4.png)


